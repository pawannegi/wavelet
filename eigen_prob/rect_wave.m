clc
clear all
close all

%--------------------------
% Parameters
%--------------------------
showit=1;

DNx = 6;
DNy = 6 ;
delx = 0.2;
dely = 0.1;

localerror= [];
Ais = [];
Nx = (2-0)/delx; % Nx = number of points. Nx = 2^p, so p = log2(Nx)
Ny = (1-0)/dely;

%Wavelet parameter px , py
px = ceil(log2(Nx));
Nx = 2^px;
py = ceil(log2(Ny));
Ny = 2^py;

%--------------------------
% Points definition
%--------------------------


DSx = 2*DNx-3; % total number of connection coefficients for a value of DN
DN2x = DNx-2;  % 2-DN <= l <= DN-2
Qx = DNx-1;    % boundary points: DN-1 at left, DN-1 at right
BCx = 2*Qx;    % add fictitious boundary points: DN-1 at left, DN-1 at right
NBCx = Nx+BCx;

% Similary fot y direction
DSy = 2*DNy-3; % total number of connection coefficients for a value of DN
DN2y = DNy-2;  % 2-DN <= l <= DN-2
Qy = DNy-1;    % boundary points: DN-1 at left, DN-1 at right
BCy = 2*Qy;    % add fictitious boundary points: DN-1 at left, DN-1 at right
NBCy = Ny+BCy;

%********************
% x dir
%********************
Ax = zeros(NBCy,NBCy+2*Qx); % D*delta_{k,l} part: Since, only when k=l 
omega02x = Concoeff_2Tuple(px, DNx,0,2);
for i = 1: NBCy
    for j = 1:DSx
        Ax(i,i+j-1) = omega02x(j) ;
    end
end

Ax = Ax(:, Qx:Qx+NBCy-1) ;  
    
%***********************
% y dir
%***********************
Ay = zeros(NBCx,NBCx+2*Qy); % D*delta_{k,l} part: Since, only when k=l
omega02y = Concoeff_2Tuple(py, DNy,0,2);
for i = 1: NBCx
    for j = 1:DSy
        Ay(i,i+j-1) = omega02y(j) ;
    end
end

Ay = Ay(:, Qy:Qy+NBCx-1) ;
%--------------------------
% Global matrix
%--------------------------
Atx = Ax';
Aty = Ay ;
A=spalloc(NBCx*NBCy,NBCx*NBCy,0);

rownum = 1;
for i = 1: NBCy
    for j = 1: NBCx
       A(rownum , j:NBCx:NBCx*NBCy) = A(rownum , j:NBCx:NBCx*NBCy) ...
           + Atx(i , :);  
       rownum = rownum + 1;
       
    end
end

rownum = 1;
for i = 1: NBCy
    for j = 1: NBCx
       
       A(rownum , (i-1)*NBCx + 1 : (i)*NBCx) = ...
           A(rownum , (i-1)*NBCx + 1 : (i)*NBCx) + Aty(j , :) ;
       rownum = rownum + 1;
       
    end
end

%--------------------------
% Apply boundary condition
%--------------------------
for i = 1: Ny
        %x bc
        row = NBCx*(i-1) + (Qy)*(NBCx) + Qx + 1 ;
        A( row , :) = 0;  
        A(row ,row) = 1;
       
        row = NBCx*NBCy - Qy*NBCx - Qx - NBCx*(i-1) ;
        A(row , :) = 0;  
        A(row, row) = 1;   
end

for i = 1: Nx
       %y bc
       row = (Qy)*(NBCx) + i + Qx  ;
       A(row , :) = 0;  
       A(row,row) = 1;
       
       row = NBCx*NBCy - Qy*NBCx - Qx - (i-1) ;
       A(row, :) = 0;  
       A(row,row) = 1;
end
 

%--------------------------
% Solve the system
%--------------------------
hsqr = 4 ;

[field,kk]=eigs(-A);

f_w = field(:,2) ;
new = reshape(f_w,NBCx,NBCy/2);

% 
figure(2 );
set(gcf, 'Position', [200, 200, 500, 500])
h = contourf(new);
colorbar