clc; clear; clear all;
%%  Metal Waveguide TM Mode Eigenvalue Analysis (Finite Difference Method)

%% Modeling information

a=2;          %  Long edge x
b=1;          %  short side y
deltax=0.02;  %  X direction discrete
deltay=0.02;  %  Y direction is discrete

% deltax=0.2;  %  X direction discrete
% deltay=0.2;  %  Y direction is discrete

%  Grid size
Nx=(a/deltax)+1-2; %not including the extreme two points i.e only intermediate nodes: 99 points
Ny=(b/deltay)+1-2; %not including the extreme two points i.e only intermediate nodes: 49 points

%% Sparse matrix establishment (line number, column number, nonzero element value)
% I is a matrix whose each column elements represents the row number of the big matrix
% (Nx*Ny)x(Nx*Ny), J is a matrix whose each column elements represents the column number of the big matrix
% (Nx*Ny)x(Nx*Ny). Each element of a column of matrix K contains the value in the (I,J) index of the big matrix (Nx*Ny)x(Nx*Ny)

I=zeros(5,(Nx*Ny)); % 5x4851
J=zeros(5,(Nx*Ny)); % 5x4851
K=zeros(5,(Nx*Ny)); % 5x4851

%  From left to right, from bottom to top

% These two for loops considers Only intermediate nodes i.e excluding all
% the corner nodes and edge nodes on all 4 sides. The numbering of the
% nodes starts from bottom left to its right and till top right node i.e
% 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22... ,Nx*Ny. This
% implementation considers that all the nodes are written in a row vector
% in the matrix.And there are 5 rows since we can have maximum of 5 nodes 
% connected to each other at one time instant (eg. consider any node that in not on corner/edge)  


for m=2:Nx-1; 
    for n=2:Ny-1; 
        index=(n-1)*Nx+m; % contains indexes of nodes belonging to intermediate nodes only one at a time. 
        I(:,index)=[index,index,index,index,index]; % save a particular index value in all 5 rows for the index column. 
        J(:,index)=[index,index+1,index-1,index+Nx,index-Nx]; % 5 adjacent nodes to the index node: eg. 11,12,10,11+Nx(top node),11-Nx(bottom node)
        K(:,index)=[(-2/deltax^2)+(-2/deltay^2),1/deltax^2,1/deltax^2,... % Enter the corresponding non-zero values in above indexed vector J.
                    1/deltay^2,1/deltay^2]; % at (i,j) we have -4/delta^2, at (i+1,j) we have 1/delta^2 and so on.. (delta=deltax=deltay)
    end
end

%  Handle the upper boundary (Edge nodes only)

%BOUNDARY CONDITIONS: E =0 at x = 0,a and y = 0,b

n=Ny; % Top boundary
for m=2:Nx-1; % excluding top corner nodes
        index=(n-1)*Nx+m; % starting from first edge point just after top left corner node till last edge node just before the top right corner
        I(:,index)=[index,index,index,index,index]; % Save corresponding index values
        J(:,index)=[index,index+1,index-1,index,index-Nx]; % 5 adjacent nodes to the indexed edge node. Mark the node just above it as INDEX.(Because we want E = 0 for all the nodes at the edges) 
        K(:,index)=[(-2/deltax^2)+(-2/deltay^2),1/deltax^2,1/deltax^2,... %  Due to boundary conditions put 0 to the node just above the indexed node
                    0,1/deltay^2]; 
end

%  Handle the lower boundary

n=1; % lower boundary
for m=2:Nx-1; % excluding left and right corners
    index=(n-1)*Nx+m; % starting from 2nd edge node to last edge point (just befor the corner node) of the bottom row
    I(:,index)=[index,index,index,index,index]; % Save corresponding index values
    J(:,index)=[index,index+1,index-1,index+Nx,index]; % 5 adjacent nodes to the indexed edge node. Mark the node just below it as INDEX. (Because we want E = 0 for all the nodes at the edges)
    K(:,index)=[(-2/deltax^2)+(-2/deltay^2),1/deltax^2,1/deltax^2,... % Due to boundary conditions put 0 to the node just below the indexed node
                1/deltay^2,0];
end

%  Handle the left border

m=1; % left boundary
for n=2:Ny-1; % all left edge nodes except the corner nodes
    index=(n-1)*Nx+m; % starting from 2nd edge node to last edge point (just befor the corner node) of the left bundary
    I(:,index)=[index,index,index,index,index]; 
    J(:,index)=[index,index+1,index,index+Nx,index-Nx]; % 5 adjacent nodes to the indexed edge node. Mark the node just left to it as INDEX. (Because we want E = 0 for all the nodes at the edges)
    K(:,index)=[(-2/deltax^2)+(-2/deltay^2),1/deltax^2,0,... % Due to boundary conditions put 0 to the node just left to the indexed node
                1/deltay^2,1/deltay^2];
end

%  Handle the right border

m=Nx; % right border
for n=2:Ny-1; % all left edge nodes except the corner nodes 
    index=(n-1)*Nx+m; % starting from 2nd edge node to last edge point (just befor the corner node) of the right boundary
    I(:,index)=[index,index,index,index,index];
    J(:,index)=[index,index,index-1,index+Nx,index-Nx]; % 5 adjacent nodes to the indexed edge node. Mark the node just right to it as INDEX. (Because we want E = 0 for all the nodes at the edges)
    K(:,index)=[(-2/deltax^2)+(-2/deltay^2),0,1/deltax^2,... % Due to boundary conditions put 0 to the node just right to the indexed node
                1/deltay^2,1/deltay^2];
end

%  Bottom left corner

m=1; 
n=1;
index=(n-1)*Nx+m; %left bottom corner node 
I(:,index)=[index,index,index,index,index];
J(:,index)=[index,index+1,index,index+Nx,index]; %5 adjacent nodes to the indexed edge node. Mark the node just left and bottom to it as INDEX. (Because we want E = 0 for all the nodes at the edges)
K(:,index)=[(-2/deltax^2)+(-2/deltay^2),1/deltax^2,0,... % % Due to boundary conditions put 0 to the node just left and below to the indexed node
            1/deltay^2,0];

%  Bottom right corner

m=Nx;
n=1;
index=(n-1)*Nx+m;
I(:,index)=[index,index,index,index,index];
J(:,index)=[index,index,index-1,index+Nx,index];
K(:,index)=[(-2/deltax^2)+(-2/deltay^2),0,1/deltax^2,...
            1/deltay^2,0];

%  Upper left corner

m=1;
n=Ny;
index=(n-1)*Nx+m;
I(:,index)=[index,index,index,index,index];
J(:,index)=[index,index+1,index,index,index-Nx];
K(:,index)=[(-2/deltax^2)+(-2/deltay^2),1/deltax^2,0,...
            0,1/deltay^2];

%  Upper right corner

m=Nx;
n=Ny;
index=(n-1)*Nx+m;
I(:,index)=[index,index,index,index,index];
J(:,index)=[index,index,index-1,index,index-Nx];
K(:,index)=[(-2/deltax^2)+(-2/deltay^2),0,1/deltax^2,...
            0,1/deltay^2];
        
%%  Eigenvalue solution

Z=sparse(I,J,K,Nx*Ny,Nx*Ny); % sparse takes an element"i" from "row" I and a corresponding element "j" from "column" J and substitutes 
                             % corresponding (i,j) belonging to (I,J) value from corresponding element K into (Nx*Ny)x(Nx*Ny) matrix.
                             % It will only store non-zero elements. To see
                             % full matrix use: full_Z_matrix = full(Z). If
                             % there are same (i,j) having different values, then sparse adds together elements in K. 
                             
[field,kk]=eigs(-Z,4,'SM');  % eigs(A,K,SIGMA) and eigs(A,B,K,SIGMA) return K eigenvalues. If SIGMA is:
% [field,kk]=eigs(-Z);         % 'LM' or 'SM' - Largest or Smallest Magnitude


%%  Comparison of eigenvalues

disp('Cutoff Wavenumber (Numerical):')
sqrt(diag(kk)).' %diag(kk) gives column vector of diagonal entries. transpose it to get row vector. (Listed in decreasing order)
disp('Cutoff Wavenumber (Analytical):')
mm=1;nn=1;
ana(1)=sqrt((mm*pi/a)^2+(nn*pi/b)^2);
mm=2;nn=1;
ana(2)=sqrt((mm*pi/a)^2+(nn*pi/b)^2);
mm=3;nn=1;
ana(3)=sqrt((mm*pi/a)^2+(nn*pi/b)^2);
mm=1;nn=2;
ana(4)=sqrt((mm*pi/a)^2+(nn*pi/b)^2);
ana(end:-1:1) % TM = 12,31,21,11

%%  Feature pattern

figure(1)
subplot(2,2,1)
pcolor(reshape(field(:,4),Nx,Ny).'); % smallest eigenvalue corresponds to smallest eigenvector and to smallest mode i.e TM11 which occurs at the 4th column (see eigenvalues location for smallest)
title('TM11')
shading interp
colorbar
subplot(2,2,2)
pcolor(reshape(field(:,3),Nx,Ny).');
title('TM21')
shading interp
subplot(2,2,3)
pcolor(reshape(field(:,2),Nx,Ny).');
title('TM31')
shading interp
subplot(2,2,4)
pcolor(reshape(field(:,1),Nx,Ny).');
title('TM12')
shading interp

%% (visualization of more modes) 
% figure(1)
% subplot(2,4,1)
% pcolor(reshape(field(:,8),Nx,Ny).'); % smallest eigenvalue corresponds to smallest eigenvector and to smallest mode i.e TM11 which occurs at the 4th column (see eigenvalues location for smallest)
% title('TM11')
% shading interp
% subplot(2,4,2)
% pcolor(reshape(field(:,7),Nx,Ny).');
% title('TM21')
% shading interp
% subplot(2,4,3)
% pcolor(reshape(field(:,6),Nx,Ny).');
% title('TM31')
% shading interp
% subplot(2,4,4)
% pcolor(reshape(field(:,5),Nx,Ny).');
% title('TM12')
% shading interp
% subplot(2,4,5)
% pcolor(reshape(field(:,4),Nx,Ny).');
% title('TM41')
% shading interp
% subplot(2,4,6)
% pcolor(reshape(field(:,3),Nx,Ny).');
% title('TM')
% shading interp
% subplot(2,4,7)
% pcolor(reshape(field(:,2),Nx,Ny).');
% title('TM')
% shading interp
% subplot(2,4,8)
% pcolor(reshape(field(:,1),Nx,Ny).');
% title('TM')
% shading interp