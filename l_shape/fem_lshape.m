function [p,A] = fem_lshape(doplot)

load matrices_verydense;

P=p;
E=e;
T=t;

[dum3 n_elements]=size(T);
[dum2 n_edges]=size(E);
[dum1 n_nodes]=size(P);

mu_relative=5000;                       %relative permeability
mu=mu_relative*4*pi*power(10,-7);       %absolute permeability
K=zeros(n_nodes,n_nodes);               %global coefficient matrix
A=zeros(1,n_nodes);                     %Magnetic vector potential
b=zeros(n_nodes,1);                     %right hand side matrix
A1=0.255;                               %boundary condition on boundary 1
A2=0;                                   %boundary condition on boundary 1


%% calculating element coefficient matrix

for element=1:n_elements
    
    nodes=t(1:3,element);
    x=p(1,nodes');
    y=p(2,nodes');    
    area=0.5*det([ ones(3,1), x', y']);

    abc=inv([ ones(3,1), x', y']);
       
    for i=1:3
        for j=1:3
            k(element,i,j)=(abc(2,j)*abc(2,i)+abc(3,j)*abc(3,i))*area/(4*mu);            
        end
    end
    
end

%% calculating global coefficient matrix

for element=1:n_elements
    nodes=T(1:3,element);
    for i=1:3
        for j=1:3            
            K(nodes(i),nodes(j))=k(element,i,j)+K(nodes(i),nodes(j));            
        end
    end
end


%% define boundary conditions
for edge=1:n_edges
    if e(5,edge)==7 || e(5,edge)==8 
        node1=e(1,edge);
        node2=e(2,edge);
        b(node1)=A1;
        b(node2)=A1;
        K(node1,:)=zeros(1,n_nodes);
        K(node1,node1)=1;
        K(node2,:)=zeros(1,n_nodes);
        K(node2,node2)=1;
    elseif e(5,edge)==3 || e(5,edge)==4 || e(5,edge)==5 || e(5,edge)==6
        node1=e(1,edge);
        node2=e(2,edge);
        b(node1)=A2;
        b(node2)=A2;
        K(node1,:)=zeros(1,n_nodes);
        K(node1,node1)=1;
        K(node2,:)=zeros(1,n_nodes);
        K(node2,node2)=1;
    end
end

%% solve
A=inv(K)*b;

%% display
elapsed_time(2)=toc;
if (doplot)
figure(1);
pdeplot(p,e,t,'xydata',A,'mesh','off','colormap','jet','colorbar','off', 'contour','on','levels',20,'title','Magnetic vector potential plot');
axis equal
end
end




