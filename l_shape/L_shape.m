function [totalerror] = L_shape(DNx , DNy , delx , dely ,doplot)
%--------------------------
% Parameters
%--------------------------
xmax = 2.25;
ymax = 2.5;

% DNx = 6;
% DNy = 8 ;
% delx = 0.2;
% dely = 0.1;

[ Nx , Ny , px , py ] = nearest_diadic( delx , dely , xmax , ymax );
%********************
% Coeff matrices
%********************
Ay = conncoffmat(py , DNy , 2 , Ny) ;
Ax = conncoffmat(px , DNx , 2 , Nx) ;
%--------------------------
% Global matrix
%--------------------------
A=globalmat( Ax , Ay );
%--------------------------
% Apply boundary condition
%--------------------------
% locating boundaries
delx = 2.25/(Nx - 1);
dely = 2.5/(Ny - 1);

bcystart = round(0.5 / dely) ;
bcxstart = round(0.75 / delx) ;

bcypnts = Ny - bcystart ;
bcxpnts = Nx - bcxstart ;

rhs = zeros(size(A,1) , 1) ;

% horizontal boundaries
val1 = zeros(Nx,1);
val2 = ones(bcxpnts + 1,1) * 0.225;
[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx ,DNy , 'y' , val1);
[A , rhs] = dirchelet_bc( A ,rhs , bcystart + 1 , Nx , Ny , DNx ,DNy ...
    , 'y' , val2 ,bcxstart   , 0 );

% vertical boundaries
val4 = zeros(Ny,1);
val5 = ones(bcypnts + 1,1) * 0.225;
[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx ,DNy , 'x' , val4);
[A , rhs] = dirchelet_bc( A ,rhs , bcxstart + 1 , Nx , Ny , DNx ,DNy ,...
    'x' , val5 , bcystart , 0);

% neumann boudnary
[ A , rhs] = neumann_bc( A ,rhs , Ny , Nx , Ny , DNx , ...
    DNy , 'y' , 1 , bcxpnts) ;
[ A , rhs] = neumann_bc( A ,rhs , Nx , Nx , Ny , DNx , ...
    DNy , 'x' , 1 , bcypnts) ;

%--------------------------
% Solve the system
%--------------------------
f_w = zeros(size(A,1),1);
condition = condest(A);
spy(A)
mssg = ['A has estimated condition number ' num2str(condition)] ;
disp(mssg);
backslash = 1;
f_w = A\rhs;
%--------------------------
% plot
%--------------------------
[ X, Y , sol ] = trimsol( f_w , Nx , Ny , DNx , DNy , xmax , ymax);

X = X(:);
Y = Y(:);
sol = sol(:);
%%-----------------------
%%delete points
%%-----------------------
toremove = zeros((bcxpnts - 2) * (bcypnts - 2) , 1) ;
index = 1 ;
for i = bcxstart+ 1 : bcxstart  + bcxpnts - 1
    for j = bcystart + 2 : bcystart + bcypnts
        toremove(index) = (i)*Ny+j ;
        index = index + 1;
    end
end

sol(toremove) = [];
X(toremove) = [];
Y(toremove) = [];
% 
%%-----------------------
%%extract boundaries
%%-----------------------
index = 1 ;
for i = 1: length(X)
    if X(i) <= 0.0 
        boundary1(index) = i;
        index = index + 1;
    end 
end
index = 1 ;
for i = 1: length(X)
    if Y(i) >= 2.5
        boundary2(index) = i;
        index = index + 1;
    end
end
index = 1 ;
for i = 1: length(X)
    if X(i) == X(boundary2(end)) && Y(i) >= 0.45
        boundary3(index) = i;
        index = index + 1;
    end
end
index = 1 ;
for i = 1: length(X)
    if Y(i) == Y(boundary3(1)) && X(i) >= 0.75
        boundary4(index) = i;
        index = index + 1;
    end
end
index = 1 ; 
for i = 1: length(X)
    if X(i) == 2.25
        boundary5(index) = i;
        index = index + 1;
    end
end
index = 1 ;
for i = 1: length(X)
    if Y(i) == 0.0
        boundary6(index) = i;
        index = index + 1;
    end
    
end

%figure(5)
boundary = [boundary1 boundary2  fliplr(boundary3) fliplr(boundary4) ...
            fliplr(boundary5) fliplr(boundary6)];
%plot(X(boundary) , Y(boundary) , '-o') ;
boundaryshift = [boundary(2:end) boundary(1)];
C = [boundary' boundaryshift'] ;

%%%-----------------------
% traingulation
%%-----------------------
tri = delaunayTriangulation(X,Y ,C);
%tri(end:end - 5,:) = []
interior = tri.isInterior();

%%-----------------------
%%error calculation with fem
%%-----------------------
[points , val] = fem_lshape(doplot) ;
femdata = griddata(points(1,:) , points(2,:), val , X , Y) ;
errorsqr = (femdata - sol).^2 ;
% remove Nan
errorsqr(isnan(errorsqr)) = [] ;
totalerror = sum(errorsqr) / length(sol) ;

%%-----------------------
%%Plotting
%%-----------------------
if(doplot)
figure(3)
trimesh(tri(interior,:) , X,Y)
v = 0:0.01:0.225 ;
figure(2 );
tricontour(tri(interior,:) , X , Y , sol , v)
%contour(X,Y,sol);
hold on
plot(X(boundary) , Y(boundary) , '-o') ;
%plot(X , Y , 'o') ;
colorbar
% end
end
end