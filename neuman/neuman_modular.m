function [errornew] = neuman_modular(DNx , DNy , delx , dely ,doplot)
%--------------------------
% Parameters
%--------------------------
xmax = 1;
ymax = 1;

% DNx = 20;
% DNy = 20 ;
% delx = 0.02;
% dely = 0.05;

[ Nx , Ny , px , py ] = nearest_diadic( delx , dely , xmax , ymax );
%********************
% Coeff matrices
%********************
Ay = conncoffmat(py , DNy , 2 , Ny) ;
Ax = conncoffmat(px , DNx , 2 , Nx) ;
%--------------------------
% Global matrix
%--------------------------
A=globalmat( Ax , Ay );
%--------------------------
% Apply boundary condition
%--------------------------
rhs = zeros(size(A,1) , 1) ;

bcy1 = zeros(Nx,1);
bcy2 = zeros(Nx,1);

[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx , ...
    DNy , 'y' , bcy1);
[A , rhs] = dirchelet_bc( A ,rhs , Ny , Nx , Ny , DNx , ...
    DNy , 'y' , bcy2);

bcx1 = ones(Ny,1) * 1.0;

[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx , ...
    DNy , 'x' , bcx1 , 1, 1);
[ A , rhs] = neumann_bc( A ,rhs , Nx , Nx , Ny , DNx , ...
    DNy , 'x' , 1 , 1);
 
%--------------------------
% Solve the system
%--------------------------
f_w = zeros(size(A,1),1);
condition = condest(A);
spy(A)
mssg = ['A has estimated condition number ' num2str(condition)] ;
disp(mssg);
backslash = 1;
f_w = A\rhs;
%--------------------------
% post processing
%--------------------------
[ X, Y , sol ] = trimsol( f_w , Nx , Ny , DNx , DNy , xmax , ymax);

analyticalsol = zeros(Ny,Nx) ;
t_bndry = 1;
    for n = 1:100
      A = 2 * t_bndry * (1 - cos(n*pi)) / (n*pi);
      analyticalsol = analyticalsol + A.*(cosh(n*pi*X) - ...
          tanh(n*pi).*sinh(n*pi*X)) .* sin(n*pi*Y);
    end

error = (analyticalsol - sol) ;
errornew = sqrt(sum(sum(error.^2)))/(Nx*Ny);
 %--------------------------
% plot
%--------------------------
if(doplot)
figure(2 );
set(gcf, 'Position', [200, 200, 1000, 500])
subplot(1,2,1);
contour(X,Y,sol,100);
% hold on
% plot(X,Y,'o');
colorbar
subplot(1,2,2);
contour(X, Y, analyticalsol,100)
% hold on
% plot(X,Y,'o');
colorbar
figure(3);
contour(X, Y, error)
colorbar
end
end