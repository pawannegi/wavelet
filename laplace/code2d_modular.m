function [errornew] = code2d_modular(DNx , DNy , delx , dely ,doplot)
%--------------------------
% Parameters
%--------------------------
xmax = 1;
ymax = 1;

% DNx = 6;
% DNy = 8 ;
% delx = 0.2;
% dely = 0.1;

[ Nx , Ny , px , py ] = nearest_diadic( delx , dely , xmax , ymax );
%********************
% Coeff matrices
%********************
Ay = conncoffmat(py , DNy , 2 , Ny) ;
Ax = conncoffmat(px , DNx , 2 , Nx) ;
%--------------------------
% Global matrix
%--------------------------
A=globalmat( Ax , Ay );
%--------------------------
% Apply boundary condition
%--------------------------
rhs = zeros(size(A,1) , 1) ;

bcy1 = zeros(Nx,1);
bcy2 = ones(Nx,1);

[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx , ...
    DNy , 'y' , bcy1);
[A , rhs] = dirchelet_bc( A ,rhs , Ny , Nx , Ny , DNx , ...
    DNy , 'y' , bcy2);

bcx1 = zeros(Ny,1);
bcx2 = zeros(Ny,1);

[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx , ...
    DNy , 'x' , bcx1);
[A , rhs] = dirchelet_bc( A ,rhs , Nx , Nx , Ny , DNx , ...
    DNy , 'x' , bcx2);
%--------------------------
% Solve the system
%--------------------------
f_w = zeros(size(A,1),1);
condition = condest(A);
spy(A)
mssg = ['A has estimated condition number ' num2str(condition)] ;
disp(mssg);
backslash = 1;
f_w = A\rhs;
%--------------------------
% post processing
%--------------------------
[ X, Y , sol ] = trimsol( f_w , Nx , Ny , DNx , DNy , xmax , ymax);

analyticalsol = 0 ;
for n = 1:2:80
    analyticalsol = analyticalsol + 4 / (n * pi) .* ...
        (sin(n*pi.*X) .* sinh(n*pi.*Y)) / sinh(n*pi) ;
end

error = (analyticalsol - sol) ;
errornew = sqrt(sum(sum(error.^2)))/(Nx * Ny);
%--------------------------
% plot
%--------------------------
if (doplot)
    figure(2 );
    set(gcf, 'Position', [200, 200, 1000, 500])
    subplot(1,2,1);
    contour(X,Y,sol);
    % hold on
    % plot(X,Y,'o');
    colorbar
    subplot(1,2,2);
    contourf(X, Y, analyticalsol)
    % % hold on
    % % plot(X,Y,'o');
    colorbar
    figure(3);
    contour(X, Y, error)
    colorbar
end
end