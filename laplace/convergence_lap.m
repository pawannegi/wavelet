% convergence study of laplace equation
clc
clear all
close all

delx = [1/8 , 1/16 , 1/32 , 1/64 ,1/128];
for i = 1:length(delx)
    error(i) = log(code2d_modular(8,8,delx(i),delx(i),0));
end
plot(log(delx) , error , '-o' , 'linewidth' , 2);
set(gca,'fontsize',10)
xlabel('log(delx)' , 'FontSize', 20)
ylabel('log(error)', 'FontSize', 20)
title('Convergence plot for laplace problem', 'FontSize', 20)
grid
cd(fileparts(which('convergence_lap')));
print('laplace_conv','-dpdf')