function [u_domain,x_domain,y_domain,L,N_x,N_y]=Case_2D_Laplace(DN,deltar)

clc;
close all;
                                            % Physical Parameters

x_out_min=0; x_out_max=1;
y_out_min=0; y_out_max=1;

deltar_x=deltar;
deltar_y=deltar;

N_x=(x_out_max-x_out_min)/deltar_x;
N_y=(y_out_max-y_out_min)/deltar_y;

                                            % Wavelet resolution m
m_x=ceil(log2(N_x));
m_y=ceil(log2(N_y));

                                            % Refined number of points
N_x=2^(m_x);
N_y=2^(m_y);

                                            % Refined resolution
                                            
rdeltar_x=(x_out_max-x_out_min)/(N_x-1);
rdeltar_y=(y_out_max-y_out_min)/(N_y-1);

DS=2*DN-3;
DN2=DN-2;               
Q=DN-1;                 % Boundary points on the left and the right
BC=2*Q;

NBC_x=N_x+BC;
NBC_y=N_y+BC;

                                            % Definition of matrices
L_x=spalloc(NBC_x,NBC_x,0);
L_y=spalloc(NBC_y,NBC_y,0);
L=spalloc((NBC_x*NBC_y),(NBC_x*NBC_y),0);
f=spalloc(NBC_x*NBC_y,1,0);
u=spalloc(NBC_x*NBC_y,1,0);
u_domain=zeros(N_x*N_y,1);
x_domain=zeros(N_x,1);
y_domain=zeros(N_y,1);

                                    % Compute the connection coefficients
omega_20_x=Concoeff_2Tuple(m_x,DN,2,0);
omega_20_y=Concoeff_2Tuple(m_y,DN,2,0);


                                            % Build the system

% Main body of the matrix

                                            % lambda_x_{i,p}^{2,0}

% Equations 1 to DN-1
for i=1:Q
    DNi=DN-i;
    for p=DNi:DS
        DNp=p-DNi+1;
        L_x(i,DNp)=L_x(i,DNp)+omega_20_x(p);
    end
end
% Equations DN to DN-1+N_x
for i=Q+1:Q+N_x         
    for p=1:DS          
        DNp=i-DN2+p-1;
        L_x(i,DNp)=L_x(i,DNp)+omega_20_x(p);
    end
end
% Equations N_x+DN to N_x+BC
DSi=DS;
for i=N_x+Q+1:NBC_x
    for p=1:DSi
        DNp=i-DN2+p-1;
        L_x(i,DNp)=L_x(i,DNp)+omega_20_x(p);
    end
    if (NBC_x-i<=Q)
        DSi=DSi-1;
    end
end


                                        %delta_y_{q,j}

D_y=speye(NBC_y);


                                        %delta_x_{i,p}
D_x=speye(NBC_x);


                                        % 1/mu*lambda_y_{q,j}^{2,0}

% Equations 1 to DN-1
for q=1:Q
    DNq=DN-q;
    for j=DNq:DS
        DNj=j-DNq+1;
        L_y(q,DNj)=L_y(q,DNj)+omega_20_y(j);
    end
end
%Equations DN to DN-1+N_y
for q=Q+1:Q+N_y         
    for j=1:DS          
        DNj=q-DN2+j-1;
        L_y(q,DNj)=L_y(q,DNj)+omega_20_y(j);
    end
end
% Equations N_y+DN to N_y+BC
DSq=DS;
for q=N_y+Q+1:NBC_y
    for j=1:DSq
        DNj=q-DN2+j-1;
        L_y(q,DNj)=L_y(q,DNj)+omega_20_y(j);
    end
    if (NBC_y-q<=Q)
        DSq=DSq-1;
    end
end


% Computation of L
L=kron(L_x,D_y)+kron(D_x,L_y);

% Incorporation of BC
node_bound_0=[];
node_bound_1=[];

for i=1:NBC_y
    for j=1:NBC_x
        if (((i-DN)*rdeltar_y)==y_out_min)&(((j-DN)*rdeltar_x)>=x_out_min)&(((j-DN)*rdeltar_x)<=x_out_max)
            node_bound_0=[node_bound_0 ((i-1)*NBC_x+j)]; %bottom edge
        end
        if (((i-DN)*rdeltar_y)==y_out_max)&(((j-DN)*rdeltar_x)>=x_out_min)&((j-DN)*rdeltar_x)<=x_out_max
            node_bound_1=[node_bound_1 ((i-1)*NBC_x+j)]; %top edge
        end
        if (((j-DN)*rdeltar_x)==x_out_min)&(((i-DN)*rdeltar_y)>=y_out_min)&(((i-DN)*rdeltar_y)<=y_out_max)
            node_bound_0=[node_bound_0 ((i-1)*NBC_x+j)]; %left edge
        end
        if (((j-DN)*rdeltar_x)==x_out_max)&(((i-DN)*rdeltar_y)==y_out_min)&(((i-DN)*rdeltar_y)<=y_out_max)
            node_bound_0=[node_bound_0 ((i-1)*NBC_x+j)]; %right edge
        end        
    end
end

len_bound_0=length(node_bound_0);
len_bound_1=length(node_bound_1);
index_bound_0=1;
index_bound_1=1;

% Boundary Conditions
for k=1:NBC_x*NBC_y
    if (index_bound_0<=len_bound_0)&(k==node_bound_0(index_bound_0))
        L(k,:)=0;
        L(k,k)=1;
        f(k)=0;
        index_bound_0=index_bound_0+1;
    end
    if (index_bound_1<=len_bound_1)&(k==node_bound_1(index_bound_1))
        L(k,:)=0;
        L(k,k)=1;
        f(k)=1;
        index_bound_1=index_bound_1+1;
    end
end
        

% Solution of the system
% u=[L]^{-1}*{f}
u=L\f;


% Extraction of the solution at the points within the domain
% index_domain=1;
for i=DN:N_y+Q
    for j=DN:N_x+Q
        u_domain((i-DN)*N_x+(j-DN+1))=u((i-1)*NBC_x+j);
    end
end
for k=1:N_x
    x_domain(k)=(k-1)*rdeltar_x;
end
for k=1:N_y
    y_domain(k)=(k-1)*rdeltar_y;
end

% Analytical solution of the system

for i = 1:length(y_domain)
    for j = 1:length(x_domain)
        u_analytical((i-1)*length(y_domain)+j,1)=0;
        if (i==length(y_domain))
            u_analytical((i-1)*length(y_domain)+j,1)=1;
            for n = 1:2:80
                u_analytical((i-1)*length(y_domain)+j,1)=u_analytical((i-1)*length(y_domain)+j,1)+((4/(n*pi))*sin(n*pi*x_domain(j)));
            end
        else
            for n = 1:2:80
                u_analytical((i-1)*length(y_domain)+j,1)=u_analytical((i-1)*length(y_domain)+j,1)+((4/(n*pi*sinh(n*pi))*sin(n*pi*x_domain(j))*sinh(n*pi*y_domain(i))));
            end
        end
    end
end


% Plotting of solution

u_dom = reshape(u_domain,N_x,N_y)';
u_dom_analytical = reshape(u_analytical,N_x,N_y)';

figure;
hold on;
axis equal;
contour(x_domain,y_domain,u_dom_analytical,150);
view(0,90);
shading interp;
colorbar;
rectangle('position',[0 0 1 1]);
title('Analytical Solution');

figure;
hold on;
axis equal;
contour(x_domain,y_domain,u_dom,150);
view(0,90);
shading interp;
colorbar;
rectangle('position',[0 0 1 1]);
title('Wavelet Solution Of Laplace Equation');

error = (u_analytical-u_domain);
error_matrix = reshape(error,N_x,N_y)';

figure;
hold on;
axis equal;
contour(x_domain,y_domain,error_matrix,150);
title('error')


% error_modified = (u_analytical(1:(N_x*(N_y-1)),1)-u_domain(1:(N_x*(N_y-1)),1));
% norm_error = norm(error,2);
% norm_error_percent = norm_error/norm(u_analytical,2)*100;
% norm_error_modified = norm(error_modified,2);
% norm_error_modified_percent = norm_error_modified/norm(u_analytical,2)*100;
% disp(['The 2 norm of the error is ' num2str(norm_error)])
% disp(['The 2 norm percent of the error is ' num2str(norm_error_percent)])
% disp(['The 2 norm of the modified error is ' num2str(norm_error_modified)]);
% disp(['The 2 norm percent of the modified error is ' num2str(norm_error_modified_percent)]);




