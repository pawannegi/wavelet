clc
clear all
close all
%--------------------------
% Parameters
%--------------------------
showit=1;

DNx = 8;
DNy = 6 ;
delx = 0.1;
dely = 0.2;

Nx = (1-0)/delx; % Nx = number of points. Nx = 2^p, so p = log2(Nx)
Ny = (1-0)/dely;

%Wavelet parameter px , py
px = ceil(log2(Nx));
Nx = 2^px;
py = ceil(log2(Ny));
Ny = 2^py;

%--------------------------
% Points definition
%--------------------------

% acommetn
DSx = 2*DNx-3; % total number of connection coefficients for a value of DN
DN2x = DNx-2;  % 2-DN <= l <= DN-2
Qx = DNx-1;    % boundary points: DN-1 at left, DN-1 at right
BCx = 2*Qx;    % add fictitious boundary points: DN-1 at left, DN-1 at right
NBCx = Nx+BCx;

% Similary fot y direction
DSy = 2*DNy-3; % total number of connection coefficients for a value of DN
DN2y = DNy-2;  % 2-DN <= l <= DN-2
Qy = DNy-1;    % boundary points: DN-1 at left, DN-1 at right
BCy = 2*Qy;    % add fictitious boundary points: DN-1 at left, DN-1 at right
NBCy = Ny+BCy;

%********************
% Y coeff matrix construction
% +2*Qy is for circular matrix construction
% it is removed after the loop
%********************
Ay = zeros(NBCy,NBCy+2*Qy); % D*delta_{k,l} part: Since, only when k=l 
%omega02x = 2^(px*2) * conn(2,DNx);
omega02y = Concoeff_2Tuple(py , DNy , 0 , 2)./(4096); %4096 is for improving condition
for i = 1: NBCy
    for j = 1:DSy
        Ay(i,i+j-1) = omega02y(j) ;
    end
end

Ay = Ay(:, Qy:Qy+NBCy-1) ;  
    
%***********************
% X coeff matrix construction
% +2*Qx is for circular matrix construction
% it is removed after the loop
%***********************
Ax = zeros(NBCx,NBCx+2*Qx); 
%omega02y = 2^(py*2) * conn(2,DNy);
omega02x = Concoeff_2Tuple(px , DNx , 0 , 2)./(4096); %4096 is for improving condition
for i = 1: NBCx
    for j = 1:DSx
        Ax(i,i+j-1) = omega02x(j) ;
    end
end

Ax = Ax(:, Qx:Qx+NBCx-1) ;
%--------------------------
% Global matrix
%--------------------------
A=spalloc(NBCx*NBCy,NBCx*NBCy,0);

rownum = 1;
for i = 1: NBCx
    for j = 1: NBCy
       A(rownum , j:NBCy:NBCx*NBCy) = A(rownum , j:NBCy:NBCx*NBCy) ...
           + Ax(i , :);  
       rownum = rownum + 1;
       
    end
end

rownum = 1;
for i = 1: NBCx
    for j = 1: NBCy
       
       A(rownum , (i-1)*NBCy + 1 : (i)*NBCy) = ...
           A(rownum , (i-1)*NBCy + 1 : (i)*NBCy) + Ay(j , :) ;
       rownum = rownum + 1;
       
    end
end

%--------------------------
% Apply boundary condition
%--------------------------
rhs = zeros(NBCx*NBCy , 1) ;
for i = 1: Nx
        %Horizontal bc
        row = NBCy*(i-1) + (Qx)*(NBCy) + Qy + 1 ;
        A( row , :) = 0;  
        A(row ,row) = 1;
        rhs(row  , 1) = 1;  
       
        row = NBCy*(i-1) + (Qx)*(NBCy) + Qy + Ny ;
        A(row , :) = 0;  
        A(row, row) = 1;
        rhs(row, 1) = 0;     
end

for i = 1: Ny
       %verticle boundary condition
       row = (Qx)*(NBCy) + Qy + i  ;
       A(row , :) = 0;  
       A(row,row) = 1;
       rhs(row, 1) = 0;
       
       row = (Qx + Nx - 1)*(NBCy) + Qy + i  ;
       A(row, :) = 0;  
       A(row,row) = 1;
       rhs(row , 1) = 0;
end
 

%--------------------------
% Solve the system
%--------------------------
f_w = zeros(NBCx*NBCy,1);
condition = condest(A);
spy(A)
mssg = ['A has estimated condition number ' num2str(condition)] ;
disp(mssg);
backslash = 1;
f_w = A\rhs; 
%--------------------------
% post processing
%--------------------------
new = reshape(f_w,[NBCy,NBCx]);
sol = new(Qy+1:NBCy-Qy , Qx+1:NBCx-Qx) ;
x = linspace(0,1,Nx);
y = linspace(0,1,Ny);
[X,Y] = meshgrid(x,y);

analyticalsol = 0 ;
for n = 1:2:80
    analyticalsol = analyticalsol + 4 / (n * pi) .* ...
        (sin(n*pi.*X) .* sinh(n*pi.*Y)) / sinh(n*pi) ; 
end

error = (analyticalsol - sol) ;
errornew = sqrt(sum(sum(error.^2)))/(Nx * Ny);
%--------------------------
% plot
%--------------------------
figure(2 );
set(gcf, 'Position', [200, 200, 1000, 500])
subplot(1,2,1);
contour(X,Y,sol);
% hold on
% plot(X,Y,'o');
colorbar
subplot(1,2,2);
contourf(X, Y, analyticalsol)
% % hold on
% % plot(X,Y,'o');
colorbar
figure(3);
contour(X, Y, error)
colorbar