clc
clear all
close all

%--------------------------
% Parameters
%--------------------------
xmax = 1;
ymax = 1;

DNx = 6;
DNy = 8 ;
delx = 0.05;
dely = 0.05;

[ Nx , Ny , px , py ] = nearest_diadic( delx , dely , xmax , ymax );
%********************
% Coeff matrices
%********************
Ay = conncoffmat(py , DNy , 2 , Ny) ;
Ax = conncoffmat(px , DNx , 2 , Nx) ;
%--------------------------
% Global matrix
%--------------------------
A=globalmat( Ax , Ay );
%--------------------------
% Apply boundary condition
%--------------------------
rhs = zeros(size(A,1) , 1) ;

bcy1 = zeros(Nx,1);
bcy2 = zeros(Nx,1);

[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx , ...
    DNy , 'y' , bcy1);
[A , rhs] = dirchelet_bc( A ,rhs , Ny , Nx , Ny , DNx , ...
    DNy , 'y' , bcy2);

bcx1 = zeros(Ny,1);
bcx2 = zeros(Ny,1);

[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx , ...
    DNy , 'x' , bcx1);
[A , rhs] = dirchelet_bc( A ,rhs , Nx , Nx , Ny , DNx , ...
    DNy , 'x' , bcx2);

bc_cap1 = ones(Ny/2,1) * 100;
bc_cap2 = ones(Ny/2,1) * -100;

[A , rhs] = dirchelet_bc( A ,rhs , Nx/4 , Nx , Ny , DNx ,...
    DNy , 'x' , bc_cap1 , Ny/4 , Ny/4);
[A , rhs] = dirchelet_bc( A ,rhs , 3*Nx/4 + 1 , Nx , Ny , DNx...
    ,DNy , 'x' , bc_cap2 , Ny/4 , Ny/4);

%--------------------------
% Solve the system
%--------------------------
f_w = zeros(size(A,1),1);
condition = condest(A);
spy(A)
mssg = ['A has estimated condition number ' num2str(condition)] ;
disp(mssg);
backslash = 1;
f_w = A\rhs;
 %--------------------------
% plot
%--------------------------
[ X, Y , sol ] = trimsol( f_w , Nx , Ny , DNx , DNy , xmax , ymax);

[Ex , Ey] = gradient(sol) ;
Ex = -Ex;
Ey = -Ey;
E = sqrt(Ex.^2 + Ey.^2) ;

figure(2 );
contour(X,Y,sol,100);
% hold on
% plot(X,Y ,'o');
colorbar

figure(3);
contour(X, Y, E,100);
colorbar
figure(4)
contour(X,Y,E,'linewidth',0.5);
hold on, quiver(X,Y,Ex,Ey,2)