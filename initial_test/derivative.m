clear all
close all
clc

x = linspace(0,2*pi,50);
y = sin(x);
c = fwt(transpose(y),4);
T = difmatrix(1,length(c),2*pi,4);
cdash = T*c;
ydash =ifwt(cdash , 4);
yapprox = ifwt(c , 4);
plot(x , ydash/2);
hold on
plot(x , yapprox);
plot(x , y);

grid on