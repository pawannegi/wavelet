close all
clear all
clc

showit=1;

DNx = 12;
DNy = 12 ;

localerror= [];
Ais = [];
Nx = 11; % Nx = number of points. Nx = 2^p, so p = log2(Nx)
Ny = 11;

%Wavelet parameter px , py
px = 6;
%Nx = 2^px;
py = 6;
%Ny = 2^py;

%--------------------------
% Points definition
%--------------------------


DSx = 2*DNx-3; % total number of connection coefficients for a value of DN
DN2x = DNx-2;  % 2-DN <= l <= DN-2
Qx = DNx-1;    % boundary points: DN-1 at left, DN-1 at right
BCx = 2*Qx;    % add fictitious boundary points: DN-1 at left, DN-1 at right
NBCx = Nx+BCx;

% Similary fot y direction
DSy = 2*DNy-3; % total number of connection coefficients for a value of DN
DN2y = DNy-2;  % 2-DN <= l <= DN-2
Qy = DNy-1;    % boundary points: DN-1 at left, DN-1 at right
BCy = 2*Qy;    % add fictitious boundary points: DN-1 at left, DN-1 at right
NBCy = Ny+BCy;

%********************
% x dir
%********************
Ax = conncoffmat( px , DNx , 2 , Nx);
%***********************
% y dir
%***********************
Ay = conncoffmat( py , DNy , 2 , Ny);
%--------------------------
% Global matrix
%--------------------------
A=globalmat(Ax , Ay);

%--------------------------
% Apply boundary condition
%--------------------------
rhs = zeros(NBCx*NBCy , 1) ;

val1 = ones(Ny,1);
val2 = zeros(Ny,1);
val3 = zeros(Nx,1);
val4 = zeros(Nx,1);
[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx ,DNy , 'y' , val2);
[A , rhs] = dirchelet_bc( A ,rhs , Nx , Nx , Ny , DNx ,DNy , 'y' , val1);

[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx ,DNy , 'x' , val3);
[A , rhs] = dirchelet_bc( A ,rhs , Ny , Nx , Ny , DNx ,DNy , 'x' , val4);

%--------------------------
% Solve the system
%--------------------------
f_w = zeros(NBCx*NBCy,1);
x_0 = f_w;
if(showit)
    condition = condest(A);
    spy(A)
    mssg = ['A has estimated condition number ' num2str(condition)] ;
    disp(mssg);
end
backslash = 1;
if(backslash)
    f_w = A\rhs; % x = A^(-1)*rhs
    
    % f_w = iterimpslvr(A,rhs);
    
else
    [L,U] = lu(A); % LU decomposition
    f_w = U\(L\rhs); % x = U^{-1}*L^{-1}*rhs
    for i=1:2
        d = rhs - A*f_w;
        e = -U\(L\d);
        f_w = f_w - e;
    end
end
%--------------------------
% plot
%--------------------------

scale = NBCx ;

new = reshape(f_w,NBCx,NBCy);
sol1 = new(Qx+1:NBCx-Qx , Qy+1:NBCy-Qy) ;

x = linspace(0,1,Ny);
y = linspace(0,1,Nx);
[X,Y] = meshgrid(x,y);

X1 = X(1:(Ny-1)/2 + 1 ,:);
Y1 = Y(1:(Ny-1)/2 + 1,:);
sol1 = sol1(1:(Ny-1)/2 + 1,:);

bc1 = sol1((Ny-1)/2 + 1 ,:) ;
bc1_new = refine_mesh(bc1) ;

DNx = 10;
DNy = 10 ;

localerror= [];
Ais = [];
Nx = 10; % Nx = number of points. Nx = 2^p, so p = log2(Nx)
Ny = length(bc1_new);

%Wavelet parameter px , py
px = 4;
%Nx = 2^px;
py = 4;
%Ny = 2^py;

%--------------------------
% Points definition
%--------------------------


DSx = 2*DNx-3; % total number of connection coefficients for a value of DN
DN2x = DNx-2;  % 2-DN <= l <= DN-2
Qx = DNx-1;    % boundary points: DN-1 at left, DN-1 at right
BCx = 2*Qx;    % add fictitious boundary points: DN-1 at left, DN-1 at right
NBCx = Nx+BCx;

% Similary fot y direction
DSy = 2*DNy-3; % total number of connection coefficients for a value of DN
DN2y = DNy-2;  % 2-DN <= l <= DN-2
Qy = DNy-1;    % boundary points: DN-1 at left, DN-1 at right
BCy = 2*Qy;    % add fictitious boundary points: DN-1 at left, DN-1 at right
NBCy = Ny+BCy;

%********************
% x dir
%********************
Ax = conncoffmat( px , DNx , 2 , Nx);
%***********************
% y dir
%***********************
Ay = conncoffmat( py , DNy , 2 , Ny);
%--------------------------
% Global matrix
%--------------------------
A=globalmat(Ax , Ay);

%--------------------------
% Apply boundary condition
%--------------------------
rhs = zeros(NBCx*NBCy , 1) ;

val1 = ones(Ny,1);
val2 = bc1_new;
val3 = zeros(Nx,1);
val4 = zeros(Nx,1);
[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx ,DNy , 'y' , val2);
[A , rhs] = dirchelet_bc( A ,rhs , Nx , Nx , Ny , DNx ,DNy , 'y' , val1);

[A , rhs] = dirchelet_bc( A ,rhs , 1 , Nx , Ny , DNx ,DNy , 'x' , val3);
[A , rhs] = dirchelet_bc( A ,rhs , Ny , Nx , Ny , DNx ,DNy , 'x' , val4);

%--------------------------
% Solve the system
%--------------------------
f_w = zeros(NBCx*NBCy,1);
x_0 = f_w;
if(showit)
    condition = condest(A);
    spy(A)
    mssg = ['A has estimated condition number ' num2str(condition)] ;
    disp(mssg);
end
backslash = 1;
if(backslash)
    f_w = A\rhs; % x = A^(-1)*rhs
    
    % f_w = iterimpslvr(A,rhs);
    
else
    [L,U] = lu(A); % LU decomposition
    f_w = U\(L\rhs); % x = U^{-1}*L^{-1}*rhs
    for i=1:2
        d = rhs - A*f_w;
        e = -U\(L\d);
        f_w = f_w - e;
    end
end
%--------------------------
% plot
%--------------------------

scale = NBCx ;

new = reshape(f_w,NBCx,NBCy);
sol2 = new(Qx+1:NBCx-Qx , Qy+1:NBCy-Qy) ;

x = linspace(0,1,Ny);
y = linspace(0.5,1.0,Nx);
[X,Y] = meshgrid(x,y);

X = X(:);
Y = Y(:);
sol2 = sol2(:);
X1 = X1(:);
Y1 = Y1(:);
sol1 = sol1(:);
X = [X ; X1] ;
Y = [Y ; Y1] ;
sol = [sol2 ; sol1];

analyticalsol = 0 ;
for n = 1:2:80
    analyticalsol = analyticalsol + 4 / (n * pi) .* ...
        (sin(n*pi.*X) .* sinh(n*pi.*Y)) / sinh(n*pi) ;
end

error = (analyticalsol - sol) ;
errornew = sqrt(sum(sum(error.^2)))/length(sol);

v = 0 :0.01 : 1;

tri = delaunay(X,Y);
tricontour(tri , X, Y , sol , v);
hold on
plot(X,Y,'o');
title('numerical')
colorbar

figure(2)
tricontour(tri , X, Y , analyticalsol , v);
hold on
plot(X,Y,'o');
title('analytical')
colorbar

v = 0 :0.0001 : 1;
figure(3)
tricontour(tri , X, Y , error , v);
colorbar