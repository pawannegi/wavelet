% at boundaries take indices l,k in the above two BC foes not reperesent
% position in the matrix A , but actually the Delta fucntion has to be
% evaluated at 0  and 1

clear all
close all
clc

d = 891;
D = 6;
delx = 1/2^D ;
x = [0:delx:1];
for i=1:D-2
    x = [x(1) - delx x x(length(x)) + delx];
end
yexact = sin(sqrt(d)*x) / sin(sqrt(d)) ;

D2=wavenp(D,length(x),2);

A = D2 + d * eye(length(x));

A1 = zeros(1 , length(x));
A1(1,D-1) = 1;
A2 = zeros(1 , length(x));
A2(1, length(x)-D+1) = 1;

A = [A1 ; A ; A2];
rhs = zeros(length(x)+2,1);
rhs(length(x)+2) = 1.0;

cu = lsqr(A , rhs);

plot(x, cu );
hold on
%plot(x , yexact)
grid on 


