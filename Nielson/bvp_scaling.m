clear all
close all
clc

alpha = 0.1 ;
D = 6
x = linspace(0,1,1024);
y = (4*pi^2 + alpha)* (sin(2 * pi * x));
yexact = sin(2 * pi * x);

cf = dst(transpose(y),D);
D2 = difmatrix(2,length(cf),1,D);
A = -D2 + alpha * eye(length(cf));
A_inv = inv(A);
cu = A_inv * cf ;
sol =idst(cu , D);

plot(x , sol);
hold on
plot(x , yexact);
grid on