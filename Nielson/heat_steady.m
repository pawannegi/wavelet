clear all
close all
clc
% Form Nielson - heat conduction equation
alpha = 0.1 ;
D = 8 ;
mu = 0.01/pi;
delt = 1/2^5 ;
x = linspace(0,1,2048);
%y = sin(2 * pi * x);
y = zeros(1,length(x));
y(x<0.5) = 1.0 ;
y(x>=0.5) = -1.0 ;
%yexact = sin(2 * pi * x);

df = fwt(transpose(y),D);
D2 = difmatrix(2,length(df),1.0,D);
D2_hat = fwt2(D2 , D);
I = eye(length(df));
Ahat = I - mu * delt * D2_hat ;
Ahat_inv = inv(Ahat);
u0 = zeros(length(df),1);
uold = fwt(u0 , D);

figure(1)
for i = 1:1
    unew = Ahat_inv * (uold + delt * df);
    uold = unew;
    res = ifwt(unew,D);
    plot(x , res );
%     axis([0 1 -1 1])
    figure(2)
    plot(unew(unew>0.05), '*');
    pause(0.5)
end

% with some numerical examples

% 
% alpha = 0.1 ;
% D = 8 ;
% mu = 0.01/pi;
% delt = 1/2^6 ;
% x = linspace(0,1,1024);
% y = sin(3 * pi * x);
% yexact = sin(2 * pi * x);
% 
% df = fwt(transpose(y),D);
% D2 = difmatrix(2,length(df),1.0,D);
% D2_hat = fwt2(D2 , D);
% I = eye(length(df));
% Ahat = I - mu * delt * D2_hat ;
% Ahat_inv = inv(Ahat);
% u0 = 5*sin(2*pi*x) + 2*sin(3*pi*x);
% uold = fwt(u0' , D);
% 
% figure(1)
% for i = 1:100
%     unew = Ahat_inv * (uold + delt * df);
%     uold = unew;
%     res = ifwt(unew,D);
%     plot(x , res );
%     
%     pause(1)
% end





