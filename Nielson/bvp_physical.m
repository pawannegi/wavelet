
%in physical space 
clear all
close all
clc

alpha = 0.1 ;
D = 6
x = linspace(0,1,1024);
y = (4*pi^2 + alpha)* (sin(2 * pi * x));
yexact = sin(2 * pi * x);

y = y';
D2 = difmatrix(2,length(y),1,D);
A = -D2 + alpha * eye(length(y));
A_inv = inv(A);
cu = A_inv * y ;

plot(x , cu);
hold on
plot(x , yexact);
grid on
