function [ A ] = conncoffmat( p , DN , order , N)
%creates the conection coefficient matrix
%   Detailed explanation goes here
% DN = Filter length
% p = scaling level
% order = derivative order
% N = Number of points

Q = DN - 1 ;
DS = 2*DN-3;
NBC = N + 2*Q ;
% coeff matrix construction
% +2*Q is for circular matrix construction
% it is removed after the loop
A = zeros(NBC,NBC+2*Q); % D*delta_{k,l} part: Since, only when k=l 
%omega02x = 2^(px*2) * conn(2,DNx);
omega02y = Concoeff_2Tuple(p , DN , 0 , order)./4096; %4096 is for improving condition
for i = 1: NBC
    for j = 1:DS
        A(i,i+j-1) = omega02y(j) ;
    end
end

A = A(:, Q:Q+NBC-1) ; 
end

