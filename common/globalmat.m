function [ A ] = globalmat( Ax , Ay )
% Ax = matrix for x direction
% Ay = matrix for y direction

NBCx = length(Ax);
NBCy = length(Ay);
A=spalloc(NBCx*NBCy,NBCx*NBCy,0);

rownum = 1;
for i = 1: NBCx
    for j = 1: NBCy
       A(rownum , j:NBCy:NBCx*NBCy) = A(rownum , j:NBCy:NBCx*NBCy) ...
           + Ax(i , :);  
       rownum = rownum + 1;
       
    end
end

rownum = 1;
for i = 1: NBCx
    for j = 1: NBCy
       
       A(rownum , (i-1)*NBCy + 1 : (i)*NBCy) = ...
           A(rownum , (i-1)*NBCy + 1 : (i)*NBCy) + Ay(j , :) ;
       rownum = rownum + 1;
       
    end
end

end

