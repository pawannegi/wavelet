function [ A ] = refine_mesh( Ainp)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

b = length(Ainp) ;
A = zeros(2 * b - 1,1);
index = 1 ;
for i = 1: b-1
   A(index) = Ainp(i) ;
   A(index + 1) = (Ainp(i) + Ainp(i+1)) / 2 ;
   index = index + 2 ;
end
   
A(index) = Ainp(b);
end

