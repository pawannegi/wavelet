function [M_L]=Moments(N,pj,d,a)


M=3*N^2-9*N+7;
sizes=2*(M+d);
offset=sizes/2;
sq2=sqrt(2);
sqpj=sqrt(2-pj);

Lm=2*N-3; %Lm=9
Lm100=Lm+offset;
N100=N+offset;

M_0=zeros(d+1,sizes);
M_L=zeros(d+1,sizes);
%N100=N+offset;
ini=1+offset;
ini1=2+offset;
D100=d+1+offset;
Q=floor((Lm100-ini)/2+ini);
L=zeros(1,sizes);
L1=[2-N:N-2]; %L=[-4,...,0,...,4]
L(ini:Lm100)=L1;

if d>0
    for k=ini:Lm100 % 1:2N-3 (2-N:N-2)
        M_0(1,k)=1; %For Daubechies wavelets
        M_L(1,k)=M_0(1,k); %\sqpj
    end
    
    for k=2:d+1 %d:d+1 (1:d)
        m_sum=0;
        for j=1:k %1:k (0:k-1)
            a_sum=0;
            for i=ini:N100 %2:N (1:N-1)
                a_sum=a_sum+a(i)*(i-ini)^(k-j); %(i-1)^((k-1)-(j-1))
            end
            factor=factorial(k-1)/(factorial(j-1)*factorial(k-j));
            m_sum=m_sum+(factor*M_0(j,Q)*a_sum);
        end
        M_0(k,Q)=1/(2*2^(k-1)-1)*m_sum;
    end
    
    for k=2:d+1 %2:d+1 (1:d)
        for li=ini:Lm100 %1:2N-3 (2-N:N-2)
            M_L(k,li)=0;
            for j=1:k %1:d+1 (0:d)
                factor=factorial(k-1)/(factorial(j-1)*factorial(k-j)); %(k+1-1)-(j-1)
                M_L(k,li)=M_L(k,li)+factor*L(li)^(k-j)*M_0(j,Q);
            end
        end
        M_L(k,:)=M_L(k,:)*2^(-pj*(k-1+.5));
    end
end
