function [qmfh]=Daubechies(n,sum_scaling)

if nargin==1
    sum_scaling=sqrt(2); % so that the total sum of the scaling coefficients add upto sqrt(2)
end
% n is even. according to literature daub2 = haar == L=1 in matlab, daub4== L=2 in matlab,daub6 = L=3 in matlab.
qmfh=dbaux(n/2,sum_scaling); 


