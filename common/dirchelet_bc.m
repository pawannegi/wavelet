function [ A , rhs] = dirchelet_bc( A ,rhs , rownum , Nx , Ny , DNx , ...
    DNy , dir , val , limit1 , limit2)
% A = global matrix
% rhs = global rhs
% rownum = row number along the direction on which BC needed
% Nx = number of pts along x
% Ny = number of pts along y
% DNx = genus along x
% DNy = genus along y
% dir = either 'x' or 'y'
% val = value of the bc

if nargin < 10
    limit1 = 0;
    limit2 = 0;
end

Qx = DNx - 1;
Qy = DNy - 1;
NBCx= Nx + 2 * Qx;
NBCy= Ny + 2 * Qy;

if (dir == 'y')
    for i = 1 + limit1 : Nx - limit2
        row = NBCy*(i-1) + (Qx)*(NBCy) + Qy + rownum ;
        A( row , :) = 0;
        A(row ,row) = 1;
        rhs(row  , 1) = val(i - limit1);
    end
end

if (dir == 'x')
    for i = 1 + limit1 : Ny - limit2
        row = (Qx + rownum - 1)*(NBCy) + Qy + i  ;
        A(row, :) = 0;
        A(row,row) = 1;
        rhs(row , 1) = val(i - limit1);
    end
end

end

