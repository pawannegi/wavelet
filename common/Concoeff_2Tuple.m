function [lmbd]=Concoeff_2Tuple(pj,N,d1,d2)
% d1,d2 = order of derivatives
% N = Type of daub eg. db2 db4, db6, etc..
% pj = order/ scale of wavelet

% eps=1-0e-30;
format long e;

% Initialisation of parameters

d=d1+d2; 

Lm=2*N-3; %Lm=9, (edited: N = 6)  
Lm1=Lm+1; %Lm1=10
Q=N-1; %Q=5

% sizes=10*N;
M=3*N^2-9*N+7;
sizes=2*(M+d);
offset=sizes/2;
ini=offset+1;

df=factorial(d)*2^(-pj*0.5);

Lm100=Lm+offset;
N100=N+offset;

L=zeros(1,sizes);
Li=L;
L1=[2-N:N-2]; %L=[-4,...,0,...4]
L(ini:Lm100)=L1;
Li(ini:Lm100)=L(ini:Lm100)+N-1; %Li=[1,...,9]

if d~=0
    %Preparing the right hand side
    rhs=zeros(Lm1,1);
    rhs(Lm1,1)=df;
    
    %Preparing the solution vector and the Moments vector
    lmbd=zeros(Lm1,1);
    
    %Preparing the Matrix A
    a=zeros(1,sizes);
    
    a(ini:N100)=Daubechies(N)*sqrt(2);
    
    A=zeros(sizes);
    
    for l=ini:Lm100
        for q=ini:Lm100
            for p=ini:N100
                A(l,q)=A(l,q)+a(p)*a(L(q)-2*L(l)+p);
            end
        end
    end
    
    dahalf=1/(2^(d-1));
    dhlf=ones(1,Lm)*dahalf;
    DH=zeros(1,sizes);
    DH(offset+1:offset+size(dhlf,2))=dhlf;
    DaHalf=diag(DH);
    A=A-DaHalf;
    clear DaHalf;
    
    %Compute the moments M and store them in place.
    
    Mom=Moments(N,pj,d,a);
    
    A(Lm100+1,:)=Mom(d+1,:);
    
    %Solve the system A*lmbd=rhs
    
    B=A(ini:Lm100+1,ini:Lm100);
    
    %This system will give \Lambda^{0,d}. To get the original coefficients,
    %we use,
    %\Lambda^{d1,d2}=(-1)^(d1)*\Lambda^{0,d} where d=d1+d2
    
    lmbd=(B\rhs)*(-1)^(d1);
    
else %This is the delta function at 1-k
    lmbd=zeros(Lm,1);
    lmbd(Q)=1;
end


