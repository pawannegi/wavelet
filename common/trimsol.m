function [ X, Y , sol ] = trimsol( f_w , Nx , Ny , DNx , DNy , xmax , ymax)

Qx = DNx - 1;
Qy = DNy - 1;
NBCx= Nx + 2 * Qx;
NBCy= Ny + 2 * Qy;

new = reshape(f_w,[NBCy,NBCx]);
sol = new(Qy+1:NBCy-Qy , Qx+1:NBCx-Qx) ;
x = linspace(0,xmax,Nx);
y = linspace(0,ymax,Ny);
[X,Y] = meshgrid(x,y);

end

