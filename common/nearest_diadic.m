function [ Nx , Ny , px , py ] = nearest_diadic( delx , dely, xmax , ymax )

Nx = (xmax-0)/delx; % Nx = number of points. Nx = 2^p, so p = log2(Nx)
Ny = (ymax-0)/dely;

%Wavelet parameter px , py
px = ceil(log2(Nx));
Nx = 2^px;
py = ceil(log2(Ny));
Ny = 2^py;

end

